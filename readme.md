# jn2-changelog
![N|Solid](https://www.jn2.com.br/wp-content/uploads/2016/06/magento-min.png)

> Toda publicação deve ser agendada e sua tag deve
> esta presente neste documento explicando todas as
> melhorias e correções provenientes dela.
>
> O formato de uma atualização deve ser: **##### [nome-da-tag] - [data-de-publicação]**

##### Utilizar as tags

![](https://lh3.google.com/u/0/d/0B01O-SQveFLvbHhsYnV4aV9XR2s=w1590-h715-iv1)

**Para instalação de novas funcionalidades novos modulos**

![](https://lh3.google.com/u/0/d/0B01O-SQveFLvVGNmNTRNZzRBOXc=w1590-h764-iv1)

**Para atualização do Magento ou de Modulos**


![](https://lh3.google.com/u/0/d/0B01O-SQveFLvVGI0MlRQb3JzdEk=w1590-h541-iv1)

**Para ajustes de layout, configurações**

![](https://lh3.google.com/u/0/d/0B01O-SQveFLveFJjWXVwdVktRDg=w1590-h541-iv1)

**Para correção de erros**

## Atualizações

---
##### v19.0.1-fix-desabilitar-stelo - 22/11/2016
![](https://lh3.google.com/u/0/d/0B01O-SQveFLvVGI0MlRQb3JzdEk=w1590-h541-iv1)

- Desativando modulo da Stelo até a Black Friday

---
##### v19.0.0-feat-modulo-stelo - 22/11/2016
![](https://lh3.google.com/u/0/d/0B01O-SQveFLvbHhsYnV4aV9XR2s=w1590-h715-iv1)

- Instalação do módulo de pagamento Stelo

---

   [dill]: <https://github.com/joemccann/dillinger>
   [git-repo-url]: <https://github.com/joemccann/dillinger.git>
   [john gruber]: <http://daringfireball.net>
   [@thomasfuchs]: <http://twitter.com/thomasfuchs>
   [df1]: <http://daringfireball.net/projects/markdown/>
   [markdown-it]: <https://github.com/markdown-it/markdown-it>
   [Ace Editor]: <http://ace.ajax.org>
   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [keymaster.js]: <https://github.com/madrobby/keymaster>
   [jQuery]: <http://jquery.com>
   [@tjholowaychuk]: <http://twitter.com/tjholowaychuk>
   [express]: <http://expressjs.com>
   [AngularJS]: <http://angularjs.org>
   [Gulp]: <http://gulpjs.com>

   [PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
   [PlGh]:  <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>
   [PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>
   [PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>
